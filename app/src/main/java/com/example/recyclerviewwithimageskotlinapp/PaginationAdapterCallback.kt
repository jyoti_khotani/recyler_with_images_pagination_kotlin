package com.example.recyclerviewwithimageskotlinapp

interface PaginationAdapterCallback {

    fun retryPageLoad()
}