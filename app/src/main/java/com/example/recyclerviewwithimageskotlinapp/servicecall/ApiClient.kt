package com.example.recyclerviewwithimageskotlinapp.servicecall

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {

    companion object{

    private val CONNECTION_TIMEOUT=( 1000*30).toLong()
        fun makeRetroFitService(): APiInterFace? {
            return Retrofit.Builder()
                    .baseUrl(APiConstants.BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(APiInterFace::class.java)

        }

        private val okHttpClient: OkHttpClient
            get() {
                val okClientBuilder = OkHttpClient.Builder()

                val httpLoggingInterceptor = HttpLoggingInterceptor()

                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                okClientBuilder.addInterceptor(httpLoggingInterceptor)

                okClientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                okClientBuilder.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                okClientBuilder.writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                return okClientBuilder.build()
            }
    }


}