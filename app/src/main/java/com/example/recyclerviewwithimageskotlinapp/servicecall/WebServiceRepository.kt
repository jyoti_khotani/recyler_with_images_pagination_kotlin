package com.example.recyclerviewwithimageskotlinapp.servicecall

import androidx.lifecycle.MutableLiveData
import com.example.recyclerviewwithimageskotlinapp.viewmodel.PlacesModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WebServiceRepository {
    val apiInterface: APiInterFace

    init {
        this.apiInterface= ApiClient.makeRetroFitService()!!
    }

    private var returnPlacesListLiveData =
        MutableLiveData<ArrayList<PlacesModel>>()

 /*return to viewModel*/
    fun returnPlaceList():MutableLiveData<ArrayList<PlacesModel>>{
        return returnPlacesListLiveData
    }

    fun loadPage(page : Int) {
        apiInterface.getPlacesList(page = page).enqueue(object :
            Callback<ArrayList<PlacesModel>> {
            override fun onFailure(call: Call<ArrayList<PlacesModel>>, t: Throwable) {
                returnPlacesListLiveData.postValue(null)
            }

            override fun onResponse(
                call: Call<ArrayList<PlacesModel>>,
                response: Response<ArrayList<PlacesModel>>
            ) {

                if (response.isSuccessful){
                    returnPlacesListLiveData.postValue(response.body())
                }else{
                    returnPlacesListLiveData.postValue(null)
                }
            }
        })
    }
}