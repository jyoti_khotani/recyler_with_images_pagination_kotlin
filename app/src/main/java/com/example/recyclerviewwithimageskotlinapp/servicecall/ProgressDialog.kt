package com.example.recyclerviewwithimageskotlinapp.servicecall

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.example.recyclerviewwithimageskotlinapp.R
import java.lang.Exception

class ProgressDialog(var context: Context) {
  private lateinit var dialog: Dialog;
   private val TIME_OUT_MILI_SECONDS: Long= 9000

    fun showprogressDialog(){
        try{
            dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window?.decorView?.setBackgroundResource(android.R.color.transparent)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.layout_popup_loader)
            dialog.show()
        }catch (exce: Exception){
            exce.printStackTrace();
        }
    }

    fun closeDialog() {
        try {
            dialog.dismiss()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}