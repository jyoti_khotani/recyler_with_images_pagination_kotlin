package com.example.recyclerviewwithimageskotlinapp.servicecall

import com.example.recyclerviewwithimageskotlinapp.viewmodel.PlacesModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface APiInterFace {

//    @GET(APiConstants.PLACES)
//    fun getPlacesList(): Call<ResponseBody>

    @GET(APiConstants.PLACES)
    fun getPlacesList(@Query("page") page: Int): Call<ArrayList<PlacesModel>>
}