package com.example.recyclerviewwithimageskotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerviewwithimageskotlinapp.databinding.ActivityMainBinding
import com.example.recyclerviewwithimageskotlinapp.servicecall.ProgressDialog
import com.example.recyclerviewwithimageskotlinapp.viewmodel.PlacesModel
import com.example.recyclerviewwithimageskotlinapp.viewmodel.PlacesViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private lateinit var placesViewModel: PlacesViewModel
    private lateinit var placesAdapter: PlacesAdapter

    private var pageOffset = 1
    private var totalPage = 10
    private var isLastPage: Boolean = false
    private var isLoading: Boolean = false

    private lateinit var progressDialog: ProgressDialog
    lateinit var placeModelList: ArrayList<PlacesModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        placesViewModel = ViewModelProvider.AndroidViewModelFactory(application).create(PlacesViewModel::class.java)
        placeModelList= ArrayList()
        initMyRecyclerView()

        loadFirstPage()
        observerDataRequest()

    }

   fun initMyRecyclerView(){
       val gridLayout=GridLayoutManager(this,2)
       placesAdapter = PlacesAdapter(this,placeModelList)
       binding.recPlaces.setHasFixedSize(true)
       binding.recPlaces.layoutManager=gridLayout
       binding.recPlaces.itemAnimator = DefaultItemAnimator()
       binding.recPlaces.adapter=placesAdapter
       pagination(gridLayout)
    }


    private fun observerDataRequest() {
        placesViewModel.observersPlace().observe(this) {
            response->
            if (totalPage == pageOffset || totalPage <= pageOffset) {
                isLastPage = true
            }

            binding.layoutLoader.zoomProgressBar.visibility = View.GONE
            if (response!=null){
                if (response.size!=0){
                    placeModelList.addAll(response)
                    placesAdapter.notifyDataSetChanged()
                }else{
//                    binding.recPlaces.visibility==View.GONE
//                    on visiblity of no data found layout

                }

            }

        }

    }
    private fun pagination(layoutManager: GridLayoutManager) {
        binding.recPlaces.addOnScrollListener(object :
            PaginationScrollListener(layoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                getMoreItems()
            }
        })
    }

    fun getMoreItems() {
        if (isLoading && pageOffset < totalPage) {
            pageOffset = pageOffset + 1

            binding.layoutLoader.zoomProgressBar.visibility = View.VISIBLE
            placesViewModel.requestFirstPlaces(pageOffset)
            isLoading = false
        } else {
            binding.layoutLoader.zoomProgressBar.visibility = View.GONE
        }
    }


    private fun loadFirstPage() {
        placesViewModel.requestFirstPlaces(pageOffset)
    }

}