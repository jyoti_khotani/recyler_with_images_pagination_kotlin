package com.example.recyclerviewwithimageskotlinapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.recyclerviewwithimageskotlinapp.servicecall.WebServiceRepository

class PlacesViewModel(application: Application):AndroidViewModel(application) {
    private var retrofitRepository: WebServiceRepository? = WebServiceRepository()

    /*initialization*/

    private var returnFirstPlacesMutableLiveData= MutableLiveData<ArrayList<PlacesModel>>()

    init {
        returnFirstPlacesMutableLiveData = retrofitRepository!!.returnPlaceList()

    }
//
    /*observers functions*/
   fun observersPlace():MutableLiveData<ArrayList<PlacesModel>>{
      return returnFirstPlacesMutableLiveData
   }

    fun requestFirstPlaces(page:Int){
        retrofitRepository!!.loadPage(page)
    }



}