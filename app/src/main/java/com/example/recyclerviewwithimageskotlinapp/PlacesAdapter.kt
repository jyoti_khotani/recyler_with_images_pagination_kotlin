package com.example.recyclerviewwithimageskotlinapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.recyclerviewwithimageskotlinapp.viewmodel.PlacesModel

class PlacesAdapter(private var context: Context,var placeModelList: ArrayList<PlacesModel>) :RecyclerView.Adapter<PlacesAdapter.ViewHolder>(),
    PaginationAdapterCallback{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder( LayoutInflater.from(parent.context).inflate(R.layout.raw_places_item, parent , false))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val placeModel = placeModelList[position]
        holder.bindItems(context , placeModel)
    }

    override fun getItemCount(): Int {
     return if (placeModelList.size > 0)
         placeModelList.size
       else 0
    }

    override fun retryPageLoad() {
        TODO("Not yet implemented")
    }

    class ViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(context: Context, placesModel: PlacesModel){
            val userImgNotificationList =  itemView.findViewById(R.id.ph01) as ImageView

            Glide.with(context).load(placesModel.download_url).placeholder(R.drawable.ic_launcher_background).into(userImgNotificationList)
        }
    }
}